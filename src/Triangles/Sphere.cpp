#define _USE_MATH_DEFINES
#include <cmath>
#include "Sphere.h"
#include <GL/glew.h>
#include <GL/freeglut.h>

Sphere::Sphere(unsigned netting):
	dataCount(0),
	indexesCount(0),
	pData(0),
	pIndexes(0)
{
	generateData(netting);
}


Sphere::~Sphere(void)
{
	glDeleteBuffers(2, vbo);
	glDeleteVertexArrays(1, &vao);
	if (pData) {
		delete[] pData;
		delete[] pIndexes;
	}
}
void Sphere::initGLBuffers(GLuint programId, const char* posName,const char* norName,const char* texName)
{
	glGenVertexArrays( 1, &vao );
	glBindVertexArray ( vao );

	glGenBuffers ( 2, &vbo[0]);
	
	glBindBuffer ( GL_ARRAY_BUFFER, vbo[0] );
	glBufferData ( GL_ARRAY_BUFFER, dataCount*sizeof(VertexData), pData, GL_STATIC_DRAW );
		
	glEnable(GL_ELEMENT_ARRAY_BUFFER);
	glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, vbo[1] );
	glBufferData ( GL_ELEMENT_ARRAY_BUFFER, indexesCount*sizeof(unsigned int), pIndexes, GL_STATIC_DRAW );
	
	int	loc = glGetAttribLocation(programId, posName);
	if (loc>-1)
	{
		glVertexAttribPointer(loc,3,GL_FLOAT,GL_FALSE,sizeof(VertexData),(GLuint *)0);
		glEnableVertexAttribArray (loc);
	}
	int loc2 = glGetAttribLocation(programId, norName);
	if (loc2>-1)
	{
		glVertexAttribPointer(loc2,3,GL_FLOAT,GL_FALSE,sizeof(VertexData),(GLuint *)(0+sizeof(float)*3));
		glEnableVertexAttribArray (loc2);
	}
	int loc3 = glGetAttribLocation(programId, texName);
	if (loc3>-1)
	{
		glVertexAttribPointer(loc3,2,GL_FLOAT,GL_FALSE,sizeof(VertexData),(GLuint *)(0+sizeof(float)*6));
		glEnableVertexAttribArray (loc3);
	}
}

void Sphere::Draw()
{
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES,indexesCount,GL_UNSIGNED_INT,0);
	glBindVertexArray(0);
}

void Sphere::generateData(unsigned netting)
{
	if (pData) {
		delete[] pData;
		delete[] pIndexes;
	}
	float sect_size = 1.0f/(netting - 1);

	dataCount = netting * netting;
	indexesCount = 6 * netting * netting;

	pData = new VertexData [dataCount];
	pIndexes = new unsigned [indexesCount];

	for (unsigned i = 0; i < netting; i++)
		for (unsigned j = 0; j < netting; j++) {
			float y = sin(M_PI*i*sect_size - M_PI/2);
			float x = cos(2*M_PI*j*sect_size)*sin(M_PI*i*sect_size);
			float z = sin(2*M_PI*j*sect_size)*sin(M_PI*i*sect_size);

			pData[i*netting + j].pos = vec3(x, y, z);
			pData[i*netting + j].nor = vec3(x, y, z);
			pData[i*netting + j].tex = vec2(1 - j*sect_size, i*sect_size);
		}

	for (unsigned i = 0; i < netting - 1; i++) 
		for (unsigned j = 0; j < netting - 1; j++) {
			pIndexes[6*(i*netting + j) + 0] = (i*netting + j);
			pIndexes[6*(i*netting + j) + 1] = ((i + 1)*netting + j);
			pIndexes[6*(i*netting + j) + 2] = ((i + 1)*netting + j + 1);
			pIndexes[6*(i*netting + j) + 3] = (i*netting + j);
			pIndexes[6*(i*netting + j) + 4] = ((i + 1)*netting + j + 1);
			pIndexes[6*(i*netting + j) + 5] = (i*netting + j + 1);
		}
}
