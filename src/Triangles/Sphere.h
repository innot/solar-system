#pragma once
#include <glm/glm.hpp>

using glm::vec3;
using glm::vec2;

//helper struct for Vertex
//contains position, normal and texture coordinates
struct VertexData
{
	 vec3 pos;
	 vec3 nor;
	 vec2 tex;
};


//some object for drawing
class Sphere
{
	VertexData* pData;	// pointer to object's internal data
	unsigned int dataCount;

	unsigned int* pIndexes;	// pointer to indexes (list of vetrices) 
	unsigned int indexesCount;
	
	unsigned int vbo[2]; // VertexBufferObjects: one for MeshVertexData, another for Indexes
	unsigned int vao; // one VertexArrayObject

public:
	Sphere(unsigned netting = 50);
	~Sphere(void);
	// function for initialization
	void initGLBuffers(unsigned int programId, const char* posName,const char* norName,const char* texName);
	// function for drawing
	void Draw();
	// function to shift sphere

private:
	// helper function generates a sphere
	void generateData(unsigned netting);

};

