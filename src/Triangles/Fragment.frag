#version 150 core

uniform sampler2D texture;
uniform vec3 camera_position;

uniform struct LightSource
{
	vec4 position;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
} light;

uniform struct Material
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
} material;

in VertexData
{
	vec3 normal;
	vec2 texcoord;
	vec3 position;
} VertexIn;

out vec4 gl_FragColor;
	
void main()
{
	vec3 normal = normalize(VertexIn.normal);
	vec3 view_direction = normalize(camera_position - VertexIn.position);
	vec3 light_direction = normalize(vec3(light.position) - VertexIn.position);

	vec4 color_coefficient = material.emission + material.ambient * light.ambient;
	float diffuse_coefficient = max(dot(normal, light_direction), 0.0);
	color_coefficient += diffuse_coefficient * material.diffuse * light.diffuse;
	float specular_coefficient = (dot(normal, -light_direction) < 0 ? max(pow(max(dot(reflect(-light_direction, normal), view_direction), 0.0), material.shininess), 0.0) : 0);
	color_coefficient += specular_coefficient * material.specular * light.specular;
	gl_FragColor = color_coefficient * vec4(texture2D(texture, VertexIn.texcoord.xy).rgb, 1);
}
