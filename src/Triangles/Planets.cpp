#include "Planets.h"
#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "EasyBMP_OpenGL.h"

const double GRAV_CONST = 6.673848e-11;
float dt = 0;
float time_multiplier = 225895; 
int last_frame_time = glutGet(GLUT_ELAPSED_TIME);

void Planet::init_texture(void) 
{
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	BMP image;
	image.ReadFromFile(texture_file);
	((EasyBMP_Texture *)texture)->ImportBMP(image);
		
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ((EasyBMP_Texture *)texture)->TellWidth(), 
		((EasyBMP_Texture *)texture)->TellHeight(), 0, GL_RGB, 
		GL_UNSIGNED_BYTE, ((EasyBMP_Texture *)texture)->Texture);
}

void Planet::init_data(GLuint n_program_id)
{
	program_id = n_program_id;
	data.initGLBuffers(program_id, "pos", "nor", "tex");
}

Planet::Planet(float r, char *texture_img, unsigned i, unsigned program_id):
	data(SPHERE_QUALITY),
	radius(r),
	angle(0),
	rotation_axis(0, 1, 0),
	offset(0, 0, 0),
	texture_file(texture_img),
	texture(0),
	texture_id(i),
	coordinates(0, 0, 0),
	speed(0, 0, 0),
	mass(0)
{
	data.initGLBuffers(program_id, "pos", "nor", "tex");
	texture = new EasyBMP_Texture;
}

Planet::Planet(void):
	data(SPHERE_QUALITY),
	radius(0),
	angle(0),
	rotation_axis(0, 1, 0),
	rotation_angle(0),
	offset(0, 0, 0),
	texture_file(0),
	texture(0),
	texture_id(0),
	coordinates(0, 0, 0),
	speed(0, 0, 0),
	mass(0),
	id(-1),
	is_planet(true)
{
	texture = new EasyBMP_Texture;
}

vec3 Planet::gravitation_function() 
{
	vec3 result(0, 0, 0);
	for (unsigned i = 0; i < N_PLANETS; i++) {
		if (i == id) continue;
		double coefficient = GRAV_CONST*(planets[i].mass)/pow(
							pow(planets[i].coordinates.x - coordinates.x, 2) +
							pow(planets[i].coordinates.y - coordinates.y, 2) +
							pow(planets[i].coordinates.z - coordinates.z, 2), 3.0/2);
		result += vec3(coefficient*(planets[i].coordinates.x - coordinates.x),
						coefficient*(planets[i].coordinates.y - coordinates.y),
						coefficient*(planets[i].coordinates.z - coordinates.z));
	}
	return result;
}

void Planet::gen_offsets() 
{
	if (!is_planet) 
		return;
	vec3 a_old = gravitation_function();
	temp_coordinates = coordinates + speed * dt; //+ a_old * dt * dt * 0.5f;
	temp_speed = speed + a_old * dt;
}

void Planet::switch_temp()
{
	coordinates = temp_coordinates;
	speed = temp_speed;
}

void Planet::gen_rotation(float delta)
{
	rotation_angle += 360.0/rotation_time/3600/24*delta; // angle per second * seconds
}

vec3 get_shifted_coordinates(vec3 coordinates) {
	if (drawing_mode) { // real mode
		return vec3(coordinates/(float)SCALING_COEF);
	} else {
		vec3 result = coordinates/(float)SCALING_COEF;
		vec3 spherical_coordinates = get_spherical_from_cartesian(result);
		if (spherical_coordinates.x > 1) 
			spherical_coordinates.x = pow(spherical_coordinates.x, 0.57); 
		if (spherical_coordinates.x < 0.0001)
			return vec3(0);
		else
			return get_cartesian_from_spherical(spherical_coordinates);
	}
}

vec3 get_scale(float radius, int id) {
	if (id == -1)
		return vec3(radius/SKYMAP_COEF);
	if (drawing_mode) { // real mode 
		return vec3(radius/RADIUS_COEF);
	} else {
		if (id == 0) // sun
			return vec3(30); // fine-tune?
		else 
			return vec3(radius/RADIUS_COEF);
	}
	// vec3(radius/(is_planet ? (id == 0 ? (drawing_mode ? RADIUS_COEF : radius/30) : RADIUS_COEF) : SKYMAP_COEF))
}

void Planet::draw(mat4x4 viewMatrix, int m_location, int n_location, int tex_location) 
{
	if (id == 0) {// this is sun
		sun_light.position = vec4(get_shifted_coordinates(coordinates), 0);
		light_source_setup(program_id, sun_light);
	}
	material_setup(program_id, material);

	mat4x4 modelMatrix = glm::translate(glm::mat4(), (is_planet ? get_shifted_coordinates(coordinates) : coordinates));
	modelMatrix = glm::rotate(modelMatrix, rotation_axis.y, vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation_angle, vec3(0, 1, 0));
	modelMatrix = glm::scale(modelMatrix, get_scale(radius, is_planet ? id : -1));

	mat4x4 modelViewMatrix = viewMatrix*modelMatrix;
	mat3x3 normalMatrix = glm::transpose(mat3x3(glm::inverse(modelMatrix)));

	glBindTexture(GL_TEXTURE_2D, texture_id);

	// glUniformMatrix4fv(mv_location, 1, 0, glm::value_ptr(modelViewMatrix));
	glUniformMatrix4fv(m_location, 1, 0, glm::value_ptr(modelMatrix));
	glUniformMatrix3fv(n_location, 1, 0, glm::value_ptr(normalMatrix));
	glUniform1ui(tex_location, 0);

	data.Draw();
}

void light_source_setup(unsigned program_id, struct LightSource &light)
{
	int pos = glGetUniformLocation(program_id, "light.position");
	int amb =  glGetUniformLocation(program_id, "light.ambient");
	int dif = glGetUniformLocation(program_id, "light.diffuse");
	int spe = glGetUniformLocation(program_id, "light.specular");
	if (pos < 0 || amb < 0 || dif < 0 || spe < 0) {
		glClearColor(0, 0, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT);
	} else {
		glUniform4fv(pos, 1, glm::value_ptr(light.position));
		glUniform4fv(amb, 1, glm::value_ptr(light.ambient));
		glUniform4fv(dif, 1, glm::value_ptr(light.diffuse));
		glUniform4fv(spe, 1, glm::value_ptr(light.specular));
	}
}

void material_setup(unsigned program_id, struct Material &material)
{
	int amb = glGetUniformLocation(program_id, "material.ambient");
	int dif = glGetUniformLocation(program_id, "material.diffuse");
	int spe = glGetUniformLocation(program_id, "material.specular");
	int emi = glGetUniformLocation(program_id, "material.emission");
	int shi = glGetUniformLocation(program_id, "material.shininess");
	if (emi < 0 || amb < 0 || dif < 0 || spe < 0 || shi < 0) {
		glClearColor(0, 0, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT);
	} else {
		glUniform4fv(amb, 1, glm::value_ptr(material.ambient));
		glUniform4fv(dif, 1, glm::value_ptr(material.diffuse));
		glUniform4fv(spe, 1, glm::value_ptr(material.specular));
		glUniform4fv(emi, 1, glm::value_ptr(material.emission));
		glUniform1fv(shi, 1, &material.shininess);
	}
}

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}