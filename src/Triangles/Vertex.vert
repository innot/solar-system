#version 150 core

uniform mat4 viewProjectionMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;

in vec3 pos;
in vec3 nor;
in vec2 tex;

out VertexData
{
	vec3 normal;
	vec2 texcoord;
	vec3 position;
} VertexOut;

void main()
{
	vec4 position = modelMatrix * vec4(pos.xyz, 1.0);
	VertexOut.texcoord = vec2(tex.xy);
	VertexOut.normal = normalMatrix * nor;
	VertexOut.position = position;
	gl_Position = viewProjectionMatrix * position;
}
